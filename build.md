#  GLPI build

## Prerequisites
- PHP 8.2
- NPM
- Composer
- MariaDB


### MariaDB
CREATE USER glpiuser@localhost IDENTIFIED BY 'toto';
GRANT ALL ON `glpi`.* TO 'glpiuser'@localhost;
GRANT SELECT ON mysql.time_zone_name TO 'glpiuser'@localhost;

## Commands
- bin/console dependencies install --composer-options="--prefer-dist --no-progress"
  - composer install --ansi --no-interaction
  - npm install --no-save
  - npm run-script build


ci.yml

init_containers-start.sh
  ghcr.io/glpi-project/githubactions-php:8.2
  ghcr.io/glpi-project/githubactions-mariadb:11.0
  ghcr.io/glpi-project/githubactions-dovecot
  ghcr.io/glpi-project/githubactions-memcached
  ghcr.io/glpi-project/githubactions-openldap
  ghcr.io/glpi-project/githubactions-redis

init_build.sh
  composer validate --strict
  bin/console dependencies install --composer-options="--prefer-dist --no-progress"
    composer install --ansi --no-interaction --prefer-dist --no-progress
    npm install --no-save
    // create .package.hash via sha1_file('package-lock.json')
    npm run-script build
  php bin/console locales:compile
    iterate on po files and run msgfmt
test_install.sh
  bin/console database:install \
  --config-dir=./tests/config --ansi --no-interaction \
  --force \
  --reconfigure --db-name=glpi --db-host=db --db-user=root \
  --strict-configuration \
  | tee $LOG_FILE
test_tests-units.sh


test_tests-functional.sh
